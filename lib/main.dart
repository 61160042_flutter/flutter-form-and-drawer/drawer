import 'package:add_a_drawer_to_a_screen/widgets/item_screen1.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const appTitle = 'Drawer Demo';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: MyHomePage(
        title: appTitle,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key,required this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState(title: title);
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState({required this.title});
  final String title;
  Widget body = Center(
    child: Text('My Page!'),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: body,
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              child: Text('Drawer Header'),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              title: const Text('item 1'),
              onTap: () {
                Navigator.pop(context);
                setState(() {
                  body = ItemScreen1();
                });
              },
            ),
            ListTile(
              title: const Text('item 2'),
              onTap: () {
                Navigator.pop(context);
                setState(() {
                  body = Center(child: Text('item 2'));
                });
              },
            ),
            ListTile(
              title: const Text('item 3'),
              onTap: () {
                Navigator.pop(context);
                setState(() {
                  body = Center(child: Text('item 3'));
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}

